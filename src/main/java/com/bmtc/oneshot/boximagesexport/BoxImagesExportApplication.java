package com.bmtc.oneshot.boximagesexport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoxImagesExportApplication {

	public static void main(String[] args) {
		SpringApplication.run(BoxImagesExportApplication.class, args);
	}
}
