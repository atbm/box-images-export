package com.bmtc.oneshot.boximagesexport.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="boxfile")
public class BoxFile {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idBoxFile;
	
	@Column(name ="owner")
	private String Owner;
	
	private String OwnerID;
	private String FilePath;
	private String FilePathID;
	private String ShortFileName;
	private String FileID;
	private String FileType;
	private String FileSize;
	private Date CreationDate;
	private Date LastModificationDate;
	private Date ImportationDate;

	public long getIdBoxFile() {
		return idBoxFile;
	}

	public void setIdBoxFile(long idBoxFile) {
		this.idBoxFile = idBoxFile;
	}

	public String getOwner() {
		return Owner;
	}

	public void setOwner(String owner) {
		Owner = owner;
	}

	public String getOwnerID() {
		return OwnerID;
	}

	public void setOwnerID(String ownerID) {
		OwnerID = ownerID;
	}

	public String getFilePath() {
		return FilePath;
	}

	public void setFilePath(String filePath) {
		FilePath = filePath;
	}

	public String getFilePathID() {
		return FilePathID;
	}

	public void setFilePathID(String filePathID) {
		FilePathID = filePathID;
	}

	public String getShortFileName() {
		return ShortFileName;
	}

	public void setShortFileName(String shortFileName) {
		ShortFileName = shortFileName;
	}

	public String getFileID() {
		return FileID;
	}

	public void setFileID(String fileID) {
		FileID = fileID;
	}

	public String getFileType() {
		return FileType;
	}

	public void setFileType(String fileType) {
		FileType = fileType;
	}

	public String getFileSize() {
		return FileSize;
	}

	public void setFileSize(String fileSize) {
		FileSize = fileSize;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Date getLastModificationDate() {
		return LastModificationDate;
	}

	public void setLastModificationDate(Date lastModificationDate) {
		LastModificationDate = lastModificationDate;
	}

	public Date getImportationDate() {
		return ImportationDate;
	}

	public void setImportationDate(Date importationDate) {
		ImportationDate = importationDate;
	}

}
