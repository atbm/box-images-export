package com.bmtc.oneshot.boximagesexport.repository.springbatch;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.bmtc.oneshot.boximagesexport.model.BoxFile;
import java.lang.String;
import java.util.List;


@Repository
public interface BoxFileRepository extends PagingAndSortingRepository<BoxFile, Long> {
    
	List<BoxFile> findByOwner(String owner);
  
}
